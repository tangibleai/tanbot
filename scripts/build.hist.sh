echo 'pip install sendgrid jupyter gunicorn django django_extensions djangorestframework django-crispy-forms redis psycopg2-binary aiohttp' >> scripts/build.sh
pip install scikit-learn
pip install spacy
pip install jupyter
hist -f | tail -n 6 > scripts/build.hist.sh
hist | tail -n 6 >> scripts/build.hist.sh
