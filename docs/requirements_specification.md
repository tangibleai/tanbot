# Requirements

0. John: Create repo "tanbot"
0. Markdown content as yml one entry (links, text) per day that Eddy was intended to send out
1. `models.py` for storing all messages/content that tabbot will say on Slack
- ContentModel: title, text, urls (many2many n ForeignKey field for urls)
- UrlModel: title (str), url (str), media_type (str)
2. Maria's Slackbot middleware can query the Tanbot API for content based on day number (1-30)
- {text: "Hi have you reviewed the qary docs", title: how to use gitlab, links: [http://tan.do.spaces/john-install-qary-tutorial.mp4, http://docs.qary.ai, ...]}
3. models.py for user log
- User: pk/id (int), slack_username, slack_id
- History: foreign key to User, datetimestamp, content foreign key
4. Webapp needs to reach out to Slackbot middleware whenever interns need reminders (based on time since last interaction)
- {user: 123, text_message: 'proactively reminding you of ...'} or "{}"
