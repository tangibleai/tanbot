# render a template.md file to HTML for pasting into an e-mail

import jinja2
from markdown import Markdown
import yaml
import sys
from pathlib import Path


def md_to_html(md, output_format='HTML', tab_length=2, extensions=['extra']):
    parser = Markdown(
        output_format=output_format,
        tab_length=tab_length,
        extensions=extensions)
    return parser.convert(md)


def template_to_md(template, **data):
    environment = jinja2.Environment()
    template_env = environment.from_string(template)
    return template_env.render(**data)


if __name__ == '__main__':
    template_path = Path('data') / '2022-12-paid-internship-email-contract.template.md'
    data_path = Path('intern.yaml')
    html_path = None

    data = dict(
        name='FIRSTNAME',
        full_name='FIRSTNAME LASTNAME',
        email='NAME@DOMAINNAME.COM',
        mentor_signed_date='Dec 10, 2022',
        dollars=10,
        hours=10,
        weeks=10,
        start="Dec 10, 2022",
        stop="Feb 20, 2023",
        preamble=(
            "Thank you for the excellent work on your miniprojects. \n"
            "We'd like to continue to grow and evolve your open source Math Quizbot that you added to our nudger repository."
        )
    )

    if len(sys.argv) > 1:
        template_path = Path(sys.argv[1])
    md_template = template_path.open().read()
    environment = jinja2.Environment()
    template = environment.from_string(md_template)

    if data_path and data_path.is_file():
        data = yaml.load(data_path.open())
    md = template.render(**data)

    parser = Markdown(
        output_format='HTML',
        tab_length=2,
        extensions=['fenced_code'])
    html = parser.convert(md)
    print(html)
    html_path = html_path or template_path.with_suffix('').with_suffix('.html')
    with html_path.open('wt') as fout:
        fout.write(html)
