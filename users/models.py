from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    slack_username = models.TextField(default='')
    fullname = models.TextField(default='')
    latest_tanbot_message_id = models.PositiveSmallIntegerField(default=1, blank=True, null=True)
    qary_skills = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'{self.user.profile.slack_username} Profile'
