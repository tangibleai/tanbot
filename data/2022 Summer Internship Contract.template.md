# {name}'s {cohort_name} Internship at {company_name} 

{prologue}Hi {name},

We are pleased to offer you an unpaid educational internship at {company_name} this {season__lower}!
Your *{cohort_name}* program starts *{start_date}* and lasts {internship_duration}.

Whether or not you accept the internship, you are welcome to join us on public [Slack](https://proai.org/sdml-slack) workspaces and [GitLab](https://gitlab.com/tangibleai/qary). We will publicly support you on open source, social-impact projects, even after {end_date}.

You will get the most out of it if you can commit to at least **10 hours a week** of study and practice for **10 weeks** (100 hours total) for this internship.

If you would like to join us this {season__lower} read the contract below and fill out your name and address at the end.
Mark the checkboxes at the end of this agreement with an "X" between the square brackets ("[ ]"). 
This helps us know that you understand the key points of this agreement.

To electronically sign this agreement, reply to this email and edit the included agreement. Overwrite the 3 placeholders in the checklist at the bottom of the agreement with your information: `__FULL_NAME__`, `__EMAIL_ADDRESS__`, and `__DATE_SIGNED__`.

-------------------------------------------------------------------------------

# {company_name} {cohort_name} Internship Agreement

This {cohort_name} Internship Agreement ("Agreement") is between {company_full_name} LLC ("{company_name}") at 3151 Via Alicante, #225, La Jolla, CA, 92037 and {full_name} ("{name}") at {email}.

The purpose of this **unpaid educational** Internship is to allow {name} to gain professional experience working on any real-world project {pronoun__lower} choose, according to the following terms and conditions:

### 1. Term:

The {cohort_name} Internship ("Internship") begins *{start_date}* and concludes *{end_date}*, unless the Internship is terminated by either party.

### 2. Purpose:

During the Term, {company_name} will provide {name} with mentorship and learning opportunities. {company_name} will continuously adjust the activities and assignments of {name} according to mutually agreed upon educational goals.

### 3. Responsibilities:

During the Term, {name}'s duties and responsibilities include:

- (a) Demonstrate honesty, punctuality, courtesy, curiosity, and a "Supercooperator" attitude; and
- (b) Engaging in the learning opportunities that {company_name} provides, to the best of {name}'s abilities;
- (c) Provide {company_name} weekly reports on their learning progress (verbal is fine).

### 4. Tasks:

{name} and {company_name} will jointly establish a schedule of meetings and tasks during the term of the Internship.

### 5. Intern position:

{name} acknowledges and agrees that {pronoun_is__lower} not engaging in the Internship as an employee and is not entitled to receive any wages or compensation for work performed or services supplied to {company_name}. 
Further {name} is responsible for all costs associated with traveling to and from {company_name} or as otherwise required in the performance of the Internship.

### 6. Acknowledgements and Representations:

- (a) {name} is not entering into this Internship for the purpose of pursuing a livelihood.
- (b) There is no expectation that the Internship will result in {company_name} employing {name}.
- (c) {company_name} agrees that {name} will not replace or displace any employee of {company_name}.
- (d) {company_name} will expend resources educating {name}. Those resources will exceed any benefit to {company_name}'s business activities.

### 7. Termination:

{company_name} and {name} may terminate the Internship without notice.

### 8. Default to Open:

Tangible AI defaults to open. Unless restricted by customer contracts or fiducial responsibility to protect user data, all Tangible AI software, concepts, data and designs are licensed under the free and open source [Hippocratic (MIT + First Do No Harm)](https://firstdonoharm.dev/) License. Interns, alumni, employees, contractors, and public contributors may freely share and reuse these designs and software packages in future projects. Listed here are examples of free open source software packages that {name} may contribute to without reservation, and may share their work freely with others:

a) [`qary`](http://gitlab.com/tangibleai/qary)
b) [`team` learning resources](http://gitlab.com/tangibleai/team)
c) [`nessvec`](http://gitlab.com/tangibleai/nessvec)
d) [`tanbot`](http://gitlab.com/tangibleai/tanbot)
e) [`nlpia2`](https://gitlab.com/prosocialai/nlpia2/)

### 9. User Data:

In the course of performing their duties under the Agreement, {name} might have access to data from the users interacting with {company_name}'s products or services ("User Data").
All right, title, and interest in User Data will remain the property of {company_name}, unless otherwise contracted. 
{name} has no intellectual property rights or other claim to User Data except where licensed under an open source license by {company_name}.
{name} will cooperate with {company_name} to protect the User Data and the users' right to privacy.
{name} will promptly notify {company_name} if {name} becomes aware of any potential infringement of those rights in accordance with the provisions of this Agreement.

### 10. Standard of Care:

{name} acknowledges and agrees that, in the course of performing their duties under the Agreement, {name} may receive or have access to Personal Information. 
{name} shall comply with the terms and conditions set forth in this Agreement in its collection, receipt, transmission, storage, disposal, use and disclosure of such Personal Information and be responsible for the unauthorized collection, receipt, transmission, access, storage, disposal, use and disclosure of Personal Information under control
In recognition of the foregoing, {name} agrees to:

- (a) keep and maintain all Personal Information in strict confidence, using such degree of care as is appropriate to avoid unauthorized access, use or disclosure;
- (b) use and disclose Personal Information solely and exclusively for the purposes for which the Personal Information, or access to it, is provided pursuant to the terms and conditions of this Agreement, and not use, sell, rent, transfer, distribute, or otherwise disclose or make available Personal Information for {name}'s own purposes without Customer's prior written consent; and
- (c) not, directly or indirectly, disclose Personal Information to any person other than {company_name}'s authorized persons, including any subcontractors, agents, outsourcers or auditors (an "Unauthorized Third Party"), without express written consent from {company_name}, unless and to the extent required by Government Authorities or as otherwise, to the extent expressly required, by applicable law.

### 11. Information Security:

{name} represents and warrants that their collection, access, use, storage, disposal and disclosure of Personal Information does and will comply with all applicable federal and state privacy and data protection laws, as well as all other applicable regulations and directives.
{name} shall implement administrative, physical and technical safeguards to protect Personal Information that are no less rigorous than accepted industry practices.

At a minimum, {name}'s safeguards for the protection of Personal Information shall include:

- (a) securing personal computer limiting access of Personal Information to Authorized Persons securing information transmission, storage and disposal; and
- (b) implementing authentication and access controls within applications that are used to store and access the Personal Information

### 12. Applicable Law:

This Agreement will be governed by California law, without giving effect to conflict of laws principles.

## Electronic Signatures

----

- [x] Agreed to by {manager_name} ({manager_email}) on {manager_date_signed}

----

- [ ] Agreed to by `__FULL_NAME__` (`__EMAIL_ADDRESS__`) on `__DATE_SIGNED__`
- [ ] I want to learn about the activities and technology of {company_name}
- [ ] I want to help others with what I learn.
- [ ] I will use a password protected PC with automatic screen lock enabled to store {company_name} data.
- [ ] I will use a web browser, such as Firefox, with at least **_Standard_ level security** protections enabled.
- [ ] I agree to make my best effort to attend at least one meeting with a {company_name} representative weekly.
- [ ] I just want to learn. I do not want to be compensated in any material way for my activities during the Internship.
- [ ] Throughout the Internship I will be kind and helpful to others at {company_name} and the communities {company_name} supports.

