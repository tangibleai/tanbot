# {cohort_name} Internship

Hi {name},

{prologue}We are pleased to offer you a spot in our unpaid educational internship this quarter!
The *{cohort_name}* program runs for **10 weeks** between *{start_date}* and *{end_date}*.

You'll get the most out of it if you can commit to at least **10 hours a week** of study and practice.

If you would like to participate please read the contract below and fill out your name and address at the end.
Check the checkboxes at the end of this agreement so that we know you understand the key points of this agreement.
To mark a checkbox in markdown, put an "X" or "x" or `*` in between the square brackets ("[ ]").
This will be your first lesson in markdown!

You may sign the Internship Agreement by replying to this email and typing your information at the bottom of the agreement to overwrite three variable names within this e-mail:

1. __FULL_NAME__
2. __EMAIL_ADDRESS__
3. __DATE_SIGNED__

-------------------------------------------------------------------------------

# {cohort_name} Internship Agreement

This Internship Agreement is between Tangible AI LLC ("Tangible AI") at 3151 Via Alicante, #225, La Jolla, CA, 92037 and {full_name} ("{name}") at {email}.

The purpose of this **unpaid educational** Internship is for {name} to gain professional experience working on real-world projects according to the following terms and conditions:

### 1. Term:

The Internship begins *{start_date}* and concludes *{end_date}*, unless the Internship ends earlier
according to other provisions in this agreement.

### 2. Purpose:

During the Term, Tangible AI will provide {name} with mentorship and learning opportunities. Tangible AI will continuously adjust the activities and assignments of {name} according to mutually agreed upon educational goals.

### 3. Responsibilties:

During the Term, {name}'s duties and responsibilities include:

    (a) Engaging in such learning opportunities as Tangible AI provides, to the best of {name}'s abilities;
    (b) Demonstrate honesty, punctuality, courtesy, a super-cooperative attitude, and a willingness to learn; and
    (c) Providing Tangible AI with weekly reports on their learning progress.

### 4. Tasks:

{name} and Tangible AI will jointly establish a schedule of meetings and tasks during the term of the Internship.

### 5. Intern position:

{name} acknowledges and agrees that he or she is not engaging in the Internship as an employee and is not entitled to receive any wages or compensation for work performed or services supplied to Tangible AI. Further {name} is responsible for all costs associated with traveling to and from Tangible AI or as otherwise required in the performance of the Internship.

### 6. Acknowledgements and Representations:

    (a) {name} is not entering into this Internship for the purpose of pursuing a livelihood.
    (b) There is no expectation that the Internship will result in Tangible AI employing {name}.
    (c) Tangible AI agrees that {name} will not replace or displace any employee of Tangible AI.
    (d) Tangible AI will expend resources educating {name}. Those resources will exceed any benefit to Tangible AI's business activities.

### 7. Termination:

Tangible AI and {name} may terminate the Internship without notice.

### 8. Confidential Information:

Intern acknowledges that {name} will gain knowledge and exposure to Tangible AI confidential and proprietary information ("Confidential Information"), which includes anything developed by {name} during the course of the Internship. {name} agrees to waive any rights {name} has to such works. Confidential Information includes records, documents, information and work in progress, and copies thereof, known or used by Tangible AI in connection with its business and affairs, including but not limited to:

    (a) information relating to any product, device, equipment, service or machine;
    (b) compilation of information (including lists of present and prospective customers and buying habits), data programs, codes, methods, techniques or processes;
    (c) information about or relating to Tangible AI's customers, employees, contractors or suppliers;
    (d) Tangible AI's business and marketing plans, present and future, including pricing and sales policies and concepts;
    (e) information about or relating to Tangible AI's potential business ventures;
    (f) financial information relating to Tangible AI and its activities; and
    (g) trade secrets, inventions, research and development, and related material.

{name} acknowledges that Confidential Information obtained by {name} in the course of the Internship shall remain the exclusive property of Tangible AI.
During the Internship or at any time thereafter, {name} agrees to treat and hold Confidential Information securely. {name} agrees not to retain, reproduce or disclose Confidential Information to any person except as may be authorized by Tangible AI for purposes relating to the Internship. {name} agrees not to disclose Confidential Information to any third party or to utilize such information in a manner that would be against the interest of Tangible AI either during or after the Term. {name} agrees to comply with all security measures and to follow all instructions formed by Tangible AI in order to safeguard and protect Confidential Information. {name} will advise Tangible AI of any unauthorized disclosure or use of Confidential Information of which {name} becomes aware at any time. Upon termination of the Agreement, or at Tangible AI's request, Intern shall deliver to Tangible AI all materials in Intern's possession relating to Tangible AI's business.
Notwithstanding anything to the contrary herein, disclosure of Confidential Information shall not be precluded if such disclosure, to regulators, is protected by law, or if such disclosure is otherwise required by law.

### 9. User Data:

In the course of performing their duties under the agreement {name} might have access to data from the users interacting with Tangible AI's Products ("User Data").
All right, title, and interest in User Data will remain the property of Tangible AI, unless otherwise contracted. Intern has no intellectual property rights or other claim to User Data that is hosted, stored, or transferred to and from the Products operated or maintained on behalf of Tangible AI. Intern will cooperate with Tangible AI to protect the User Data and the users’ right to privacy. Intern will promptly notify Tangible AI if Intern becomes aware of any potential infringement of those rights in accordance with the provisions of this Agreement.

### 10. Standard of Care:

Intern acknowledges and agrees that, in the course of performing their duties under the agreement, Intern may receive or have access to Personal Information. Intern shall comply with the terms and conditions set forth in this Agreement in its collection, receipt, transmission, storage, disposal, use and disclosure of such Personal Information and be responsible for the unauthorized collection, receipt, transmission, access, storage, disposal, use and disclosure of Personal Information under control
In recognition of the foregoing, Intern agrees to:

    (a) keep and maintain all Personal Information in strict confidence, using such degree of care as is appropriate to avoid unauthorized access, use or disclosure;
    (b) use and disclose Personal Information solely and exclusively for the purposes for which the Personal Information, or access to it, is provided pursuant to the terms and conditions of this Agreement, and not use, sell, rent, transfer, distribute, or otherwise disclose or make available Personal Information for Intern's own purposes without Customer's prior written consent; and
    (c) not, directly or indirectly, disclose Personal Information to any person other than Tangible AI's authorized persons, including any subcontractors, agents, outsourcers or auditors (an "Unauthorized Third Party"), without express written consent from Tangible AI, unless and to the extent required by Government Authorities or as otherwise, to the extent expressly required, by applicable law.

### 11. Information Security:

Intern represents and warrants that their collection, access, use, storage, disposal and disclosure of Personal Information does and will comply with all applicable federal and state privacy and data protection laws, as well as all other applicable regulations and directives.
Intern shall implement administrative, physical and technical safeguards to protect Personal Information that are no less rigorous than accepted industry practices.

At a minimum, Intern's safeguards for the protection of Personal Information shall include:

    (a) securing personal computer limiting access of Personal Information to Authorized Persons securing information transmission, storage and disposal; and
    (b) implementing authentication and access controls within applications that are used to store and access the Personal Information

### 12. Applicable Law:

This Agreement will be governed by California law, without giving effect to conflict of laws principles.

## Electronic Signatures

----

- [x] Agreed to by {manager_name} ({manager_email}) on {manager_date_signed}

----

- [ ] Agreed to by __FULL_NAME__ (__EMAIL_ADDRESS__) on __DATE_SIGNED__
- [ ] I want to learn about the activities and technolgoy of Tangible AI and help others with what I learn.
- [ ] I have a password-protected PC I will use to store Tangible AI data. Automatic screen lock is enabled.
- [ ] I will use a browser, such as Firefox, with at least **_Standard_ level security** protections enabled.
- [ ] I agree to make my best effort to attend at least one meeting with a Tangible AI representative weekly.
- [ ] I do not desire to be compensated in any material way for my activities during the Internship.
