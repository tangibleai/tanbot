# {cohort_name} Internship

Hi {name},

Welcome aboard the {cohort_name} internship! We have a diverse group of {len(num_interns)} interns this quarter from around the globe ({intern_locations}).

This bot will interact with you [on the Tangible AI slack]({slack_url}) in the `#interns` channel. Join us there to colaborate with your fellow interns. You'll also see us discussing Tangible AI projects on the `#general` channel there.

If you're in the middle of {school_break_name} here are some tasks to get you inspired:

- [ ] join us on [slack]({slack_url})
- [ ] sign up with a free account on gitlab.com (no need to chose any of the fancy trial subscriptions)
- [ ] fork (or clone and branch) the intern ():
- [ ] check out previous intern project reports [here](https://gitlab.com/tangibleai/team/-/tree/master/learning-resources/projects/reports)
Also, you may want to start checking out these projects for ideas:
