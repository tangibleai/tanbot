# Welcome to the {cohort_name} Internship {name}

Hi {name},

Welcome aboard!

Your first assignment is to create a 30 minute one-on-one weekly meeting with me (Hobson) on [my calendar](https://calendly.com/hobs).

Next you'll want to add our [weekly mob programming](https://proai.org/mobprog-cal) to your calendar.

Tangible AI staff (and bots) will interact with you [on the San Diego Machine Learning slack](https://proai.org/sdml-slack) in the `#qary` channel.
You can collaborate with your fellow interns and the Tangible AI community there.

## Optional Slack Workspaces

Here are some more Slack Workspaces to join if you want to collaborate with others working on python or social-impact projects.

- [SD Python Slack](https://proai.org/sdpug-slack)
- [NextSD Slack](https://proai.org/nextsd-slack)

## Gitlab

You'll need a GitLab account to be able to share your code with the world.
GitLab is 100% open source and socially responsible, unlike GitHub (Microsoft).

1. Sign up with a free account on gitlab.com (no need to chose any of the fancy trial subscriptions)
2. Star & fork the `team` repo: [gitlab.com/tangibleai/team](https://gitlab.com/tangibleai/team)
3. Check out previous intern [project reports]({intern_reports_url})
4. Star & fork the `qary` repo (https://gitlab.com/tangibleai/qary)
5. Check out the [`qary` docs](https://docs.qary.ai)

### More GitLab Repos

Star & fork any of [our repositories](https://gitlab.com/tangibleai/) that interest you:

- [`nessvec`](https://gitlab.com/tangibleai/nessvec): an NLP project
- [`nudger`](https://gitlab.com/tangibleai/nudger): an SMS chatbot for community college students
- [`tanbot`](http://gitlab.com/tangibleai/tanbot): a Telegram and e-mail bot for social impact (and sending interns e-mails ;)
- [`nlpia2`](https://gitlab.com/tangibleai/nlpia2) (ask us about the private `nlpia-manuscript` repo)

## Zoom Meetings

Here are some Google Calendar invitations for meetings you can attend to see how an Agile organization works:




