# Welcome to the {{cohort_name}} Internship {{name}}

Hi {{name}},

Welcome aboard!

Your first assignment is to create a 30 minute one-on-one meeting with me (Hobson) on [my calendar]({{calendly_url}}).
Select a time and that you will be available for the next several weeks.

Tangible AI staff (and bots) will interact with you on Slack in the `#interns` channel here [{{social_networks[0]['name']}}](social_networks[0]['url']).

You may want to also join these social networks (Discord, Slack, and Mastodon) to brainstorm with Hobson and other developers:

{% for net in social_networks[1:] %}
- [ {{ net['name'] }} ]( {{ net['url'] }} )
{% endfor %}

## Gitlab

You'll need a GitLab account to be able to share your code with the world.
GitLab is 100% open source and socially responsible, unlike GitHub (Microsoft).
Here is your first of the weekly exercises to [Get Started with Git]({{exercise}})
Here's an overview of the exercise:

1. Sign up with a free account on gitlab.com (no need to chose any of the fancy trial subscriptions)
2. Star & fork the `team` repo: [gitlab.com/tangibleai/team](https://gitlab.com/tangibleai/team)
3. Check out previous intern [project reports]({{intern_reports_url}})
4. Create a markdown file in the intern project report directory to hold your notes for the next two weeks.
5. Star & fork the `qary` repo (https://gitlab.com/tangibleai/qary)
6. Check out the [`qary` docs](https://docs.qary.ai)

### More GitLab Repos

Star & fork any of [our repositories](https://gitlab.com/tangibleai/) that interest you:

- [`nessvec`](https://gitlab.com/tangibleai/nessvec): an NLP project
- [`nudger`](https://gitlab.com/tangibleai/nudger): an SMS chatbot for community college students
- [`tanbot`](http://gitlab.com/tangibleai/tanbot): a Telegram and e-mail bot for social impact (and sending interns e-mails ;)
- [`nlpia2`](https://gitlab.com/tangibleai/nlpia2) (ask us about the private `nlpia-manuscript` repo)