# {cohort_name} Internship

Hi {name},

{prologue}We are pleased to offer you a spot in our unpaid educational internship this quarter!
The *{cohort_name}* program runs for **10 weeks** from *{start_date}* to *{end_date}*.

You are welcome to join us on [Slack](https://proai.org/tangibleai-slack) and [GitLab](https://gitlab.com/tangibleai/qary) to begin brainstorming about your project before {start_date}.
And we will continue to support you on open source, social-impact projects after {end_date}, granting you reduced access to {company_name} resources.

You'll get the most out of it if you can commit to at least **10 hours a week** of study and practice.

If you would like to participate you'll want to read the contract below and fill out your name and address at the end and type your **signature**: your full legal name and your main email address. You will want to check several checkboxes at the end of this agreement so we know you understand the key points of this agreement. You can check a checkbox in markdown by putting an "X" or "x" or `*` in between the square brackets ("[ ]") at the end of this agreement. This will be your first lesson in markdown!

You may sign the Internship Agreement by replying to this email and typing your information at the bottom of the agreement to overwrite the 4 variables: _FULL_NAME_, _EMAIL_ADDRESS_, _DATE_SIGNED_

-------------------------------------------------------------------------------

# {cohort_name} Internship Agreement

This Internship Agreement is between {company_name} LLC ("{company_name}") at 3151 Via Alicante, #225, La Jolla, CA, 92037 and {full_name} ("Intern") at {email}.

The purpose of this **unpaid educational** Internship is for {full_name} to gain professional experience working on real-world projects according to the following terms and conditions:

### 1. Term:

The Internship begins *{start_date}* and concludes *{end_date}*, unless the Internship ends earlier
according to other provisions in this agreement.

### 2. Purpose:

During the Term, {company_name} will provide the Intern with mentorship and learning opportunities. {company_name} will continuously adjust the activities and assignments of the Intern according to mutually agreed upon educational goals.

### 3. Responsibilties:

During the Term, the Intern’s duties and responsibilities include:

    (a) Engaging in such learning opportunities as {company_name} provides, to the best of the Intern’s abilities;
    (b) Demonstrate honesty, punctuality, courtesy, a super-cooperative attitude, and a willingness to learn; and
    (c) Providing {company_name} with weekly reports on their learning progress.

### 4. Tasks:

{name} and {company_name} will jointly establish a schedule of meetings and tasks during the term of the Internship.

### 5. Intern position:

{name} acknowledges and agrees that {pronoun_is} not engaging in the Internship as an employee. {name} is not entitled to receive any wages or compensation for work performed or services supplied to {company_name}. {company_name} will not compensate {name} for any travel, internet service, or cloud services costs incurred by {name}'s one their personal or business accounts outside of {company_name}'s control.

### 6. Acknowledgements and Representations:

- (a) The Intern is not entering into this Internship for the purpose of pursuing a livelihood.
- (b) There is no expectation that the Internship will result in {company_name} employing the Intern.
- (c) {company_name} agrees that the Intern will not replace or displace any employee of {company_name}. 
- (d) {company_name} will expend resources educating the Intern that exceed the net benefit to {company_name}'s business activities from this internship.

### 7. Termination:

{company_name} may at any time in her sole discretion terminate the Internship without notice. {name} may also terminate the Internship for any reason.

### 8. Confidential Information:

{name} acknowledges that the {pronoun} will gain knowledge and exposure to {company_name} confidential and proprietary information ("Confidential Information"), which includes anything developed by the Intern during the course of the Internship. The Intern agrees to waive any rights the Intern has to such works, including moral and proprietary rights, and to execute whatever documentation is required to affect the same. Confidential Information includes records, documents, information and work in progress, and copies thereof, known or used by {company_name} in connection with its business and affairs, including but not limited to:

- (a) information relating to any product, device, equipment, service or machine;
- (b) compilation of information (including lists of present and prospective customers and buying habits), data programs, codes, methods, techniques or processes;
- (c) information about or relating to {company_name}’s customers, employees, contractors or suppliers;
- (d) {company_name}’s business and marketing plans, present and future, including pricing and sales policies and concepts;
- (e) information about or relating to {company_name}’s potential business ventures;
- (f) financial information relating to {company_name} and its activities; and
- (g) trade secrets, inventions, research and development, and related material.

The Intern acknowledges that Confidential Information obtained by the Intern in the course of the Internship shall remain the exclusive property of {company_name}.
During the Internship or at any time thereafter, the Intern agrees to treat and hold Confidential Information securely. The Intern agrees not to retain, reproduce or disclose Confidential Information to any person except as may be authorized by {company_name} for purposes relating to the Internship. The Intern agrees not to disclose Confidential Information to any third party or to utilize such information in a manner that would be against the interest of {company_name} either during or after the Term. The Intern agrees to comply with all security measures and to follow all instructions formed by {company_name} in order to safeguard and protect Confidential Information. The Intern will advise {company_name} of any unauthorized disclosure or use of Confidential Information of which the Intern becomes aware at any time. Upon termination of the Agreement, or at {company_name}’s request, Intern shall deliver to {company_name} all materials in Intern's possession relating to {company_name}’s business.
Notwithstanding anything to the contrary herein, disclosure of Confidential Information shall not be precluded if such disclosure, to regulators, is protected by law, or if such disclosure is otherwise required by law.

### 9. User Data:

In the course of performing their duties under the agreement the Intern might have access to data from the users interacting with {company_name}’s Products (“User Data”).
All right, title, and interest in User Data will remain the property of {company_name}, unless otherwise contracted. Intern has no intellectual property rights or other claim to User Data that is hosted, stored, or transferred to and from the Products operated or maintained on behalf of {company_name}. Intern will cooperate with {company_name} to protect the User Data and the users’ right to privacy. Intern will promptly notify {company_name} if Intern becomes aware of any potential infringement of those rights in accordance with the provisions of this Agreement.

### 10. Standard of Care:

Intern acknowledges and agrees that, in the course of performing their duties under the agreement, Intern may receive or have access to Personal Information. Intern shall comply with the terms and conditions set forth in this Agreement in its collection, receipt, transmission, storage, disposal, use and disclosure of such Personal Information and be responsible for the unauthorized collection, receipt, transmission, access, storage, disposal, use and disclosure of Personal Information under control
In recognition of the foregoing, Intern agrees to:

    (a) keep and maintain all Personal Information in strict confidence, using such degree of care as is appropriate to avoid unauthorized access, use or disclosure;
    (b) use and disclose Personal Information solely and exclusively for the purposes for which the Personal Information, or access to it, is provided pursuant to the terms and conditions of this Agreement, and not use, sell, rent, transfer, distribute, or otherwise disclose or make available Personal Information for Intern’s own purposes without Customer’s prior written consent; and
    (c) not, directly or indirectly, disclose Personal Information to any person other than {company_name}’s authorized persons, including any subcontractors, agents, outsourcers or auditors (an “Unauthorized Third Party”), without express written consent from {company_name}, unless and to the extent required by Government Authorities or as otherwise, to the extent expressly required, by applicable law.

### 11. Information Security:

Intern represents and warrants that their collection, access, use, storage, disposal and disclosure of Personal Information does and will comply with all applicable federal and state privacy and data protection laws, as well as all other applicable regulations and directives.
Intern shall implement administrative, physical and technical safeguards to protect Personal Information that are no less rigorous than accepted industry practices.

At a minimum, Intern's safeguards for the protection of Personal Information shall include:

    (a) securing personal computer limiting access of Personal Information to Authorized Persons securing information transmission, storage and disposal; and
    (b) implementing authentication and access controls within applications that are used to store and access the Personal Information

### 12. Applicable Law:

This Agreement will be governed by California law, without giving effect to conflict of laws principles.

## Electronic Signatures

----

- [x] Agreed to by Hobson Lane (hobson@tangibleai.com) on Jan 16, 2021

----

- [ ] Agreed to by _INTERN_FULL_NAME_ (_EMAIL_ADDRESS_) on _DATE_SIGNED_
- [ ] I want to learn about the activities and technolgoy of {company_name} and help others with what I learn.
- [ ] I have a password-protected PC I will use to store {company_name} data. Automatic screen lock is enabled.
- [ ] I will use a browser, such as Firefox, with at least **_Standard_ level security** protections enabled.
- [ ] I agree to make my best effort to attend at least one meeting with a {company_name} representative weekly.
- [ ] I do not desire to be compensated in any material way for my activities during the Internship.
