# `tanbot`

The `tanbot` Django webapp deployed on digital ocean droplet to be the brain of the Intern chatbot


### Developer install

First retrieve a copy of the source code for `tanbot`:

```bash
git clone git@gitlab.com:tangibleai/tanbot.git
cd tanbot
```

Then, install and use the `conda` python package manager within the [Anaconda](https://www.anaconda.com/products/individual#Downloads) software package.

```bash
conda create -y -n tanbotenv 'python>=3.7,<3.9'
source activate tanbotenv
pip install -r requirements.txt
pip install --editable .
```

While in development you can provide initial data with fixtures for models

```bash
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata onboard_User_data.json
python manage.py loaddata onboard_SheduledMessage_data.json
python manage.py runserver
```

Steps to deploy

```bash
ssh root@159.65.100.20
cd /app/tanbot
git pull
docker-compose -f docker-compose.prod.yml up -d --force-recreate --build web
```

If ERROR: Could not install packages due to an OSError: [Errno 28] No space left on device

```bash
docker system prune -a
```

## Using Tanbot to Send E-mail Contracts

Edit your `template.md` file in `internal/team/tanbot/data/` and add the appropriate data/variables to the `all-internships.yml` file in the same directory.
Then provide the paths to your template and mail merge data like this:

```bash
$ workon tanbot  # you'll need to have our workon script installed
$ python src/tanbot/emailer.py \
    --cohort=../internal/team/tanbot/data/all-internships.yml \
    --template=../internal/team/tanbot/data/'2021 Fall Internship Contract.template.md'
```
