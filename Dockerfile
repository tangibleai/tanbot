
# pull official base image
FROM python:3.7

# set work directory
WORKDIR /tanbot

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apt-get update
RUN apt-get install python3-dev libpq-dev -y

# lint
RUN pip install --upgrade pip
RUN pip install flake8
COPY . .
RUN flake8 --ignore=E501,F401,F541 .

# install dependencies
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# copy entrypoint.sh
COPY ./scripts/entrypoint.sh .

# run entrypoint.sh
ENTRYPOINT ["scripts/entrypoint.sh"]
