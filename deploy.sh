#!/bin/sh

# ssh -o StrictHostKeyChecking=no root@$DIGITAL_OCEAN_IP_ADDRESS << 'ENDSSH'
#   mkdir -p /app
#   # # backup database from host to timestamp host
#   # [ -f "db/db.sqlite3" ] && cp db/db.sqlite3 "db/db.sqlite3_$(date +'%F %T')"
#   cd /app
#   # # backup database from existing docker image with latest user data to host
#   # docker exec -i app_web_1 sh -c 'cat src/db.sqlite3' | cat > ~/db/db.sqlite3
#   export $(cat .env | xargs)
#   docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY_USER
#   docker pull $IMAGE:web
#   docker pull $IMAGE:nginx
#   docker pull $IMAGE:celery
#   docker pull $IMAGE:celery-beat
#   docker-compose -f docker-compose.prod.yml up -d
#   # # copy database with latest user data from host to overwrite new docker image database
#   # cat ~/db/db.sqlite3 | docker exec -i app_web_1 sh -c 'cat > src/db.sqlite3'
#   docker exec -i app_web_1 sh -c 'python manage.py collectstatic --noinput && python manage.py makemigrations && python manage.py migrate'
#   docker exec app_web_1 sh -c 'python manage.py loaddata onboard_SheduledMessage_data.json datadump00.json'
# ENDSSH
