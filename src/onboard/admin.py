from django.contrib import admin

from .models import Message, SheduledMessage


admin.site.register(Message)
admin.site.register(SheduledMessage)
