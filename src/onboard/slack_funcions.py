import logging
import os
# Import WebClient from Python SDK (github.com/slackapi/python-slack-sdk)

from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# WebClient insantiates a client that can call API methods
# When using Bolt, you can use either `app.client` or the `client` passed to listeners.
OAUTH_ACCESS_TOKEN = os.environ.get('OAUTH_ACCESS_TOKEN')
client = WebClient(token=OAUTH_ACCESS_TOKEN)

# client = WebClient(token=SLACK_BOT_TOKEN)
# ID of user you watch to fetch information for
# user_id = "U0198540HD0"


def display_name_normalized(user_id):
    """
    >>> display_name_normalized("U0198540HD0")
    'John May'
    >>> display_name_normalized("RANDOM--45fdsbh")
    """
    try:
        # Call the users.info method using the WebClient
        result = client.users_info(
            user=user_id
        )
        logger.info(result)

    except SlackApiError as e:
        logger.error("Error fetching conversations: {}".format(e))
        return None

    return result['user']['profile']['display_name_normalized']
