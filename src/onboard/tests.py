from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import Client
from django.urls import reverse
from django.utils import timezone

from .models import Message, SheduledMessage


class HomeViewTests(TestCase):
    """ tests for home """

    def test_not_logged_in(self):
        """ is redirected if not logged in """

        response = self.client.get(reverse('onboard:onboard-home'))
        self.assertEqual(response.status_code, 302)

    def setUp(self):
        ''' setup a test client '''

        self.client = Client()
        self.user = User.objects.create_user(
            'intern1', 'intern1@interns.com', 'internpassword')

    def test_logged_in(self):
        """ The home page loads properly if logged in"""

        self.client.login(username='intern1', password='internpassword')
        response = self.client.get(reverse('onboard:onboard-home'))
        self.assertEqual(response.status_code, 200)


class MessageCreateViewTests(TestCase):
    """ tests for tanbot view """

    def test_message_not_logged_in(self):
        """ is redirected if not logged in """

        response = self.client.get(reverse('onboard:onboard-tanbot'))
        self.assertEqual(response.status_code, 302)

    def setUp(self):
        ''' setup a test client '''

        self.client = Client()
        self.user = User.objects.create_user(
            'Tanbot', 'intern1@interns.com', 'internpassword')

    def test_message_logged_in(self):
        """ The Tanbot page loads properly if logged in"""

        self.client.login(username='Tanbot', password='internpassword')
        response = self.client.get(reverse('onboard:onboard-tanbot'))
        self.assertEqual(response.status_code, 200)


class TanbotApiViewTests(TestCase):
    """ tests for tanbot_api view """

    def setUp(self):
        ''' setup a test client '''

        self.client = Client()
        self.user = User.objects.create_user(
            'Tanbot', 'intern1@interns.com', 'internpassword')

    def test_tanbot_api_loads_properly(self):
        """ The Tanbot api loads properly """

        response = self.client.get(
            reverse('onboard:tanbot-api', kwargs={'username': 'Tanbot'}))
        self.assertEqual(response.status_code, 200)


class MessageModelTests(TestCase):
    """ tests for Message Model """

    def test_message_is_created(self):
        """test Message is created"""

        self.client = Client()
        self.user = User.objects.create_user(
            'Tanbot', 'intern1@interns.com', 'internpassword')
        self.assertEqual(Message.objects.filter(
            sent_from=self.user).last(), None)
        message = Message.objects.create(sent_to=User.objects.get(username=self.user),
                                         sent_from=User.objects.get(
                                             username='Tanbot'),
                                         message_text='test')
        message.save()
        self.assertEqual(Message.objects.filter(
            sent_from=self.user).last().message_text, 'test')


class SheduledMessageModelTests(TestCase):
    """ tests for SheduledMessage Model """

    def test_sheduled_message_is_created(self):
        """test SheduledMessage is created"""

        self.client = Client()
        self.user = User.objects.create_user(
            'Tanbot', 'intern1@interns.com', 'internpassword')
        self.assertEqual(SheduledMessage.objects.filter(
            sent_to=self.user).last(), None)
        message = SheduledMessage.objects.create(sent_to=User.objects.get(username=self.user),
                                                 scheduled_time=timezone.now(),
                                                 message_text='test')
        message.save()
        self.assertEqual(SheduledMessage.objects.filter(
            sent_to=self.user).last().message_text, 'test')
