""" map views to a URL - URLconf """

from django.conf.urls import url
from django.shortcuts import render
from django.urls import path

from .views import MessageCreateView, BroadcastMessageCreateView
from . import views

app_name = 'onboard'

urlpatterns = [
    path('', views.home, name='onboard-home'),
    path('messages/', MessageCreateView.as_view(), name='onboard-tanbot'),
    url(r'^api/(?P<username>.+)$', views.tanbot_api, name='tanbot-api'),
    path('slack/', views.slack_verification_challenge),
    path('broadcast/', BroadcastMessageCreateView.as_view(), name='onboard-broadcast'),
]
