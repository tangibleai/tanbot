import json
import os
import pathlib

from celery import shared_task
from celery.decorators import periodic_task
from celery.task.schedules import crontab
from django.utils import timezone
from django.contrib.auth.models import User
from slack_sdk import WebClient

from .models import Message

if os.environ.get('VERIFICATION_TOKEN'):
    VERIFICATION_TOKEN = os.environ.get('VERIFICATION_TOKEN')
    OAUTH_ACCESS_TOKEN = os.environ.get('OAUTH_ACCESS_TOKEN')
    CLIENT_ID = os.environ.get('CLIENT_ID')
    CLIENT_SECRET = os.environ.get('CLIENT_SECRET')

if pathlib.Path('/etc/config.json').is_file():
    with open('/etc/config.json') as config_file:
        config = json.load(config_file)
    VERIFICATION_TOKEN = config['VERIFICATION_TOKEN']
    OAUTH_ACCESS_TOKEN = config['OAUTH_ACCESS_TOKEN']
    CLIENT_ID = config['CLIENT_ID']
    CLIENT_SECRET = config['CLIENT_SECRET']


@shared_task
def send_sheduled_message():
    client = WebClient(token=OAUTH_ACCESS_TOKEN)
    now = timezone.now()
    nowish = f'{now.year} {now.month} {now.day} {now.hour} {now.minute}'
    messages = Message.objects.all()
    for message in messages:
        message_time = message.date_scheduled
        message_nowish = f'{message_time.year} {message_time.month} {message_time.day} {message_time.hour} {message_time.minute}'
        if message_nowish == nowish and message.message_title == 'scheduled':
            users = User.objects.all()
            for user in users:
                # if user.username == '<@U0198540HD0>':
                if user.username.startswith('<@'):
                    # client.chat_postMessage(channel='@U0198540HD0', text=message.message_text)
                    username = user.username.strip('<>')
                    client.chat_postMessage(
                        channel=username, text=message.message_text)
        if message_nowish == nowish and message.message_title == 'broadcast':
            client.chat_postMessage(
                channel='#interns', text=message.message_text)

        if message_nowish == nowish and not message.message_title:
            client.chat_postMessage(
                channel=f'{message.sent_to}'.strip('<>'), text=message.message_text)

        if message_nowish == nowish and message.message_title == 'test':
            # client.chat_postMessage(
            #     channel='#interns', text=message.message_text)
            tanbot_message = Message(sent_to=User.objects.get(username='JohnMay'),
                                     sent_from=User.objects.get(username='Tanbot'),
                                     message_text=message.message_text)

            tanbot_message.save()

# @shared_task
# def send_daily_message():
#     # client = slack.WebClient(token=OAUTH_ACCESS_TOKEN)
#     now = timezone.now()
#     messages = Message.objects.all()
#     for message in messages:
#         if message.message_title == 'daily':
#             # client.chat_postMessage(channel=user.username, text=text)


# @periodic_task(run_every=crontab(hour=9, minute=30, day_of_week=1))
# https://docs.celeryproject.org/en/2.0-archived/getting-started/periodic-tasks.html

            #  every minute of every day
@periodic_task(run_every=(crontab(minute='*/1')))
def run_send_sheduled_message():
    send_sheduled_message.delay()

    #  9:30am every mon-fri
# @periodic_task(run_every=(crontab(hour='9', minute='30', day_of_week='1-5')))
# def run_send_daily_message():
#     send_daily_message.delay()
