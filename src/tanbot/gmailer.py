""" Check gmail inbox and download emails and headers

References:
  * https://www.geeksforgeeks.org/how-to-read-emails-from-gmail-using-gmail-api-in-python/

"""
# for encoding/decoding messages in base64
import base64
from bs4 import BeautifulSoup
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from mimetypes import guess_type as guess_mime_type
from pathlib import Path
import pickle
import pandas as pd
import sys

try:
    from .constants import DATA_DIR
except ImportError:
    DATA_DIR = Path(__file__).resolve().parent.parent.parent / 'data'

from pathlib import Path

# from django.conf import settings
import logging

log = logging.getLogger(__name__)

# Define the SCOPES. If modifying it, delete the token.pickle file.
SCOPES = [
    'https://www.googleapis.com/auth/gmail.readonly',
    'https://www.googleapis.com/auth/gmail.send']
DEFAULT_TOKEN_PICKLE_PATH = Path(DATA_DIR) / 'secrets-gmail-token-cache.pickle'
DEFAULT_CREDENTIALS_JSON_PATH = Path(DATA_DIR) / 'secrets-gmail.json'


def build_message(destination, body='', subject='', attachments=[], our_email='us@example.com'):
    if not attachments:  # no attachments given
        message = MIMEText(body)
        message['to'] = destination
        message['from'] = our_email
        message['subject'] = subject
    else:
        message = MIMEMultipart()
        message['to'] = destination
        message['from'] = our_email
        message['subject'] = subject
        message.attach(MIMEText(body))
        for filename in attachments:
            add_attachment(message, filename)
    return {'raw': base64.urlsafe_b64encode(message.as_bytes()).decode()}


def add_attachment(message, filename):
    """ Add attachment with the given filename to the given message """

    content_type, encoding = guess_mime_type(filename)
    if content_type is None or encoding is not None:
        content_type = 'application/octet-stream'
    main_type, sub_type = content_type.split('/', 1)
    if main_type == 'text':
        fp = open(filename, 'rb')
        msg = MIMEText(fp.read().decode(), _subtype=sub_type)
        fp.close()
    elif main_type == 'image':
        fp = open(filename, 'rb')
        msg = MIMEImage(fp.read(), _subtype=sub_type)
        fp.close()
    elif main_type == 'audio':
        fp = open(filename, 'rb')
        msg = MIMEAudio(fp.read(), _subtype=sub_type)
        fp.close()
    else:
        fp = open(filename, 'rb')
        msg = MIMEBase(main_type, sub_type)
        msg.set_payload(fp.read())
        fp.close()
    filename = Path(filename).basename
    msg.add_header('Content-Disposition', 'attachment', filename=filename)
    message.attach(msg)


class GmailAccount():
    default_token_pickle_path = DEFAULT_TOKEN_PICKLE_PATH
    default_credentials_json_path = DEFAULT_CREDENTIALS_JSON_PATH

    def __init__(self, service=None, token_pickle_path=None, credentials_json_path=None):
        if isinstance(service, GmailAccount):
            self.service = self.connect_gmail_service(
                token_pickle_path=(token_pickle_path or service.token_pickle_path),
                credentials_json_path=(token_pickle_path or service.credentials_json_path),
            )
        if service is None:
            self.service = self.connect_gmail_service(
                token_pickle_path=(token_pickle_path or self.default_token_pickle_path),
                credentials_json_path=(credentials_json_path or self.default_credentials_json_path))

    def connect_gmail_service(self,
                              token_pickle_path=DEFAULT_TOKEN_PICKLE_PATH,
                              credentials_json_path=DEFAULT_CREDENTIALS_JSON_PATH,
                              scopes=SCOPES):
        """ Read tokens from cache (token_pickle_path) or refresh tokens if necessary

        >>> connect_gmail_service()
        """
        token_pickle_path = Path(token_pickle_path or DEFAULT_TOKEN_PICKLE_PATH)
        creds = None

        # The file token.pickle contains the user access token.
        # Check if it exists
        if Path(token_pickle_path).exists():

            # Read the token from the file and store it in the variable creds
            with open(token_pickle_path, 'rb') as token:
                creds = pickle.load(token)

        # If credentials are not available or are invalid, ask the user to log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    credentials_json_path, SCOPES)
                creds = flow.run_local_server(port=0)

            # Save the access token in token.pickle file for the next run
            with open(token_pickle_path, 'wb') as token:
                pickle.dump(creds, token)

        service = build('gmail', 'v1', credentials=creds)
        return service

    def get_emails(self, max_emails=1, token_pickle_path=DEFAULT_TOKEN_PICKLE_PATH, scopes=SCOPES):
        """ Read tokens from cache (token_pickle_path) or refresh tokens if necessary

        >>> emails = get_emails(1)
        >>> len(emails) == 1
        True
        """
        # Variable creds will store the user access token.
        # If no valid token found, we will create one.
        service = self.connect_gmail_service(token_pickle_path=token_pickle_path, scopes=scopes)

        # Connect to the Gmail API

        result = service.users().messages().list(userId='me', maxResults=max_emails).execute()
        # result = service.users().messages().list(userId='me', maxResults=200).execute()

        messages = result.get('messages')

        # messages is a list of dictionaries where each dictionary contains a message id.

        # iterate through all the messages
        emails = []
        print(f'len(messages): {len(messages)}')
        for msg in messages:
            # Get the message from its id
            txt = service.users().messages().get(userId='me', id=msg['id']).execute()

            # Use try-except to avoid any Errors
            # Get value of 'payload' from dictionary 'txt'
            payload = txt['payload']
            headers = payload['headers']

            em = {}
            for d in headers:
                em[str(d['name']).lower()] = d['value']

            # The Body of the message is in Encrypted format. So, we have to decode it.
            # Get the data and decode it with base 64 decoder.
            parts = payload.get('parts', [{}])[0]
            data = parts.get('body', {}).get('data', '')
            data = data.replace("-", "+").replace("_", "/")
            decoded_data = base64.b64decode(data)
            soup = BeautifulSoup(decoded_data, "lxml")
            em['body'] = '' if soup.body is None else soup.body()
            emails.append(em)
        return pd.DataFrame(emails)

    def send_message(self, destination, subject, body, attachments=[], service=None):
        """ Send email (destination, subject, body, attachments) using provided service obj """
        api_payload = build_message(destination, subject=subject, body=body, attachments=attachments)

        self.service = service or self.service or self.connect_to_gmail_service()

        request = self.service.users().messages().send(userId="me", body=api_payload)
        response = request.execute()
        return response


if __name__ == '__main__':
    max_emails = 1
    if len(sys.argv) > 1:
        max_emails = int(sys.argv[1])
    gm = GmailAccount(our_email='hobson@tangibleai.com')
    emails = gm.get_emails(max_emails=1)
    print(f'Retrieved {len(emails)} emails.')
