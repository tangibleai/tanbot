# -*- coding: utf-8 -*-
from pkg_resources import get_distribution, DistributionNotFound

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = __name__
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:
    __version__ = 'unknown'
finally:
    del get_distribution, DistributionNotFound

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .tanbot_celery import app as celery_app

__all__ = ('celery_app',)
