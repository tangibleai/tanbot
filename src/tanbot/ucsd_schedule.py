""" Download and parse UCSD Schedule in upcoming quarters to update all-internships.yaml """
import pandas as pd
import re
import yaml
from datetime import timedelta, datetime
from dateutil import parser
import logging

from pathlib import Path

from constants import ALL_INTERNSHIPS_FILEPATH

log = logging.getLogger(__name__)
parse_datetime = parser.parse

WEEKDAY_NAME = 'Mon Tue Wed Thu Fri Sat Sun'.split()


def get_ucsd_table(url='https://blink.ucsd.edu/instructors/resources/academic/calendars/2021.html'):
    df = pd.read_html(url)[0]

    table = [tuple([-1] + list(df.columns))] + list(df.to_records())

    # print('\n'.join(str(len(x)) for x in table))
    table = [[i] + list(row)[1:] for (i, row) in enumerate(table)]

    return table


def weekday_to_int(weekday):
    if isinstance(weekday, str):
        weekday = weekday.strip().lower()[:3].title()
        return WEEKDAY_NAME.index(weekday)
    return int(weekday)


def to_datetime(dt):
    if isinstance(dt, str):
        return parse_datetime(dt.strip())
    if isinstance(dt, (list, tuple)) and len(dt) > 2:
        return datetime(*list(dt))
    if isinstance(dt, dict):
        return datetime(**dt)
    return dt


def previous_weekday(dt, weekday):
    weekday = weekday_to_int(weekday)
    dt = to_datetime(dt)
    return dt - timedelta((dt.weekday() - weekday) % 7)


def table_to_records(table):
    quarters = []
    q = {}
    for row in table:
        i, txt, date = row
        txt = txt.strip().lower().rstrip('s')
        date = date.strip().lower()
        if re.match(r'[\w ]+\s+2\d{1,3}', txt):
            quarters.append(q)
            q = dict()
            q['name'] = txt.title()
            q['year'] = int(txt.split()[-1])
        elif txt.endswith('begin') and 'ucsd_start' not in q:
            q['ucsd_start'] = date
            quarters[-1]['ucsd_end'] = date
            dt = parse_datetime(date)
            start_saturday = previous_weekday(dt, 'Sat')
            q['start'] = start_saturday.isoformat()[:10]
            q['end'] = (start_saturday + timedelta(10 * 7 - 1)).isoformat()[:10]
            q['presentations'] = (dt + timedelta(11 * 7 - 1)).isoformat()[:10]
        elif txt.endswith('xam'):
            q['ucsd_exam'] = date
    return quarters


def update_all_internships(filepath=ALL_INTERNSHIPS_FILEPATH, table=None):
    if table is None:
        table = get_ucsd_table()
    table_records = table_to_records(table)
    ucsd_quarters = {}
    for row in table_records:
        year = int(row.get('year', 0))
        season = str(row.get('season', '')).title()
        row['season'] = season
        row['year'] = year
        ucsd_quarters[(year, season)] = row

    internships = yaml.full_load(Path(filepath).open())
    for i, internship in enumerate(internships):
        # name = internship['cohort_name']
        year = int(internship['year'])
        try:
            season = str(internship['season']).strip().title()
        except KeyError:
            # log.error(f'Unable to find season in internship #{i} (0 offset)')
            # print(list(internship.keys()))
            raise
        q = ucsd_quarters.get((year, season))
        if q:
            internship['year'] = year
            internship['season'] = season
            internship['cohort_name'] = f'{season.title()} {int(year)}'
            internship.update(q)
        else:
            print(f'Couldnt find {(year, season)} in {ucsd_quarters.keys()}')

    # yaml.dump(internships, open(filepath, 'w'))
    return internships


if __name__ == '__main__':
    updated_internships = update_all_internships()
