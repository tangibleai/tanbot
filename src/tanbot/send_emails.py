#!/usr/bin/env python3
""" 2 weeks before start of internship quarter
    run this script to send all accepted interns a contract


"""
import argparse
import constants
# DEFAULT_TEMPLATE_FILENAME
# from .constants import DEFAULT_COHORT_DATA_PATH
from pathlib import Path
from emailer import send_emails_sendgrid, load_template
import yaml

import sys
print(sys.argv)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Send emails to interns or score internship applications.')
    parser.add_argument('--template', metavar='TEMPLATE_PATH', type=Path,
                        default=Path(constants.DEFAULT_TEMPLATE_FILENAME),
                        help='Filepath or filename (within DATA_DIR) for email template.')
    parser.add_argument('--cohort', metavar='COHORT_DATA_PATH', type=Path,
                        default=Path(constants.DEFAULT_COHORT_DATA_PATH),
                        help='Filepath or filename for yaml file containing the internship and contact info for interns.')
    args = parser.parse_args()
    cohort_data = yaml.full_load(args.cohort.open())
    if isinstance(cohort_data, list):
        cohort_data = cohort_data[0]
    send_emails_sendgrid(
        cohort_data=cohort_data,
        template=load_template(Path(args.template)))
